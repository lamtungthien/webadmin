﻿using ManagePetSHop.Entites;
using ManagePetSHop.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class CustomerRepository : Repository<Customer>
    {
        public CustomerRepository() : base()
        {

        }
        public bool CreateCustomer(Customer customer)
        {
            var checkCreate = false;
            try
            {
                checkCreate = this.Create(customer);
               
                return checkCreate;
            }
            catch
            {
                return checkCreate;
            }
           

        }
        public Customer GetCustomerByFacebookEmail(string facebookEmail)
        {
            var customer =  this.Get(q => q.FaceBookAccount.Equals(facebookEmail)).FirstOrDefault();
            return customer;
        }
        public Customer GetById(int Id)
        {
            
            var customer = base.GetById(Id);
            return customer;
        }
        public Customer GetCustomerByPhone(string phone)
        {
            var customer = this.Get(q => q.AccountPhone.Equals(phone));
            return customer.FirstOrDefault();
        }
    }
}