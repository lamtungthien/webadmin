﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class StoreRepository : Repository<Store>
    {
        public StoreRepository() : base() { }
        public List<Store> GetAllStore()
        {
            return this.GetAll();
        }
        public List<Store> GetStoreByBrand(int brandId)
        {
            var result = this.Get(q => q.BrandId == brandId);
            return result;
        }
    }
}