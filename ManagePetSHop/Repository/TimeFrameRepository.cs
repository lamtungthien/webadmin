﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class TimeFrameRepository : Repository<TimeFrame>
    {
        public TimeFrameRepository() : base()
        {

        }
        public TimeFrame GetTimeFramByServiceAndDate(int serviceId, DateTime date)
        {
            var result = this.Get(q => q.Active == true && q.DateTime == date && q.IsOrder == false).FirstOrDefault();
            return result;
        }
        public List<TimeFrame> GetTimeFrameByService()
        {
            var result = this.Get(q => q.Active == true
                                
                                && q.IsOrder == false);
            return result;
        }
        public TimeFrame GetTimeFrameByTime(int timeId)
        {
            return this.Get(q => q.TimeId == timeId).FirstOrDefault();
        }
    }
}