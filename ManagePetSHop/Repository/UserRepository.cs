﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class UserRepository
    {
        public Customer FindByFacebookEmail(string facebookEmail)
        {
            using (var context = new ManagePetShopEntities())
            {
                // find customer by facebook email 
                var customer = context.Set<Customer>().Where(q => q.FaceBookAccount.Equals(facebookEmail)).FirstOrDefault();
                if(customer != null)
                {
                    return customer;
                }
            }
            return null;
        }
    }
}