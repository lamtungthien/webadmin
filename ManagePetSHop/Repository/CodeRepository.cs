﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class CodeRepository : Repository<Code>
    {
        public CodeRepository() : base() { }
        public bool CreateCode(Code code)
        {
            bool check = false;
            try
            {
                check =  this.Create(code);
                return check;
            }
            catch
            {
                return check;
            }
        }
    }
}