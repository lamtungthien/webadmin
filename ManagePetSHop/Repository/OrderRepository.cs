﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class OrderRepository : Repository<Order>
    {
        public OrderRepository() : base()
        {

        }
        public bool CreateOrder(Order order)
        {
            bool check = false;
            try
            {
                check = this.Create(order);
                return true;
            }
            catch
            {
                return check;
            }
        }
        public Order GetByOrder(string invoiceOrder)
        {
            var result = this.Get(q => q.InvoiceId.Equals(invoiceOrder)).FirstOrDefault();
            return result;
        }
        public Order GetByOrderId(int orderId)
        {
            var result = this.Get(q => q.Id == orderId).FirstOrDefault();
            return result;
        }
        public List<Order> GetByCustomerAndStatus(int customerId, int status)
        {
            var result = this.Get(q => q.CustomerId == customerId && q.Status == status);
            return result;
        }
        public List<Order> GetByCustomer(int customerId)
        {
            var result = this.Get(q => q.CustomerId == customerId);
            return result;
        }
    }
}