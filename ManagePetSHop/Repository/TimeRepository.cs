﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class TimeRepository : Repository<Time>
    {
        public Time GetTimeById(int timeId)
        {
            var result = this.Get(q => q.Id == timeId).FirstOrDefault();
            return result;
        }
    }
}