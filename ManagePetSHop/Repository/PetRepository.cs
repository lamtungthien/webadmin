﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class PetRepository : Repository<Pet>
    {
        public List<Pet> GetPetByBrand(PetBrand petbrand)
        {
            var pet = this.Get(q => q.PetBrands == petbrand);
            return pet;
        }
    }
}