﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class OptionalRepository : Repository<Optional>
    {
        public Optional GetOptionalByService(int serviceId)
        {
            var result = this.Get(q => q.ServiceId == serviceId).FirstOrDefault();
         
            return result;
        }
    }
}