﻿using ManagePetSHop.Entites;
using ManagePetSHop.RepositoryInterface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace ManagePetSHop.Repository
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        ManagePetShopEntities context;
        public Repository()
        {
            ManagePetShopEntities context2 = new ManagePetShopEntities();
            this.context = context2;
        }
        public bool Create(TEntity entity)
        {
            try
            {
                context.Set<TEntity>().Add(entity);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
                Console.WriteLine("{0}", e.Message);
            }
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public List<TEntity> GetAll()
        {
            return context.Set<TEntity>().ToList();
        }

        public TEntity GetById(int Id)
        {
            return context.Set<TEntity>().Find(Id);
        }

        public TEntity GetByString(string parameter)
        {
            return context.Set<TEntity>().Find(parameter);
        }
        public List<TEntity> Get(Expression<Func<TEntity, bool>> ex)
        {
            var result =  context.Set<TEntity>().Where(ex).ToList();
            return result;
        }
        public void Update(TEntity entity)
        {
            context.Entry(entity).State = EntityState.Modified;
        }
        //public TEntity Get<TEntity>(string parameter)
        //{
        //    var enumerable = (IEnumerable<TEntity>)(typeof(TEntity).GetProperty(parameter).GetValue(context, null));
        //    return enumerable.FirstOrDefault();
        //}

      

      

    }
}