﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class PetBrandRepository : Repository<PetBrand>
    {
        public List<Pet> GetPetBrandByBrand(int brandId)
        {
            //var brand = this.Get(q => q.BrandId == brandId);
            var petbrand = this.Get(q => q.BrandId == brandId).ToList();
            var pet = petbrand.Select(q => q.Pet).ToList();
            return pet;
        }
    }
}