﻿using ManagePetSHop.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Repository
{
    public class ServiceRepository : Repository<Service>
    {
        public List<Service> GetServiceByBrand(int brandId)
        {
            var result = this.Get(q => q.BrandId == brandId);
            return result;
        }
    }
}