﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagePetSHop.RepositoryInterface
{
    interface IRepository<T> where T:class
    {
        List<T> GetAll();
        T GetById(int Id);
        bool Create(T entity);

        void Delete(T entity);

        void Update(T entity);
        T GetByString(string parameter);
    }
}
