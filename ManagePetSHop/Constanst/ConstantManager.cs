﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Constanst
{
    public static class ConstantManager
    {
        public const string CodeGenError = "Xảy ra lỗi khi tạo mã code. Vui lòng thử lại";
        public const string OrderGenError = "Xảy ra lỗi khi tạo đơn hàng. Vui lòng thử lại";
        public const string OrderSuccess = "Tạo đơn hàng thành công";
        public const string LoginFbError = "Lỗi đăng nhập với facebook";
        public const string LoginPhoneError = "Lỗi đăng nhập với phone";
    }
}