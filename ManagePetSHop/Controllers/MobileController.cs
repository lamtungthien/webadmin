﻿using Facebook;
using ManagePetSHop.Constanst;
using ManagePetSHop.Entites;
using ManagePetSHop.Repository;
using ManagePetSHop.Ultis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Newtonsoft.Json;
namespace ManagePetSHop.Controllers
{
    public class MobileController : Controller
    {

        public JsonResult test()
        {
            return Json("test nhé anh thịnh", JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoginByFacebook(string fbAccessToken)
        {
            try
            {
                CustomerRepository customerApi = new CustomerRepository();

                var fb = new FacebookClient();
                fb.AccessToken = fbAccessToken;
                dynamic me = fb.Get("me?fields=id,link,name,picture.width(200).height(200),email");
                string fbId = me.id;
                string fbEmail = me.email;
                string fbName = me.name;
                bool? fbGender = null;
                string fbpicUrl = "";
                var jsonPic = me.picture;
                //get picture
                var customer = customerApi.GetCustomerByFacebookEmail(fbEmail);
                fbpicUrl = (string)(((JsonObject)(((JsonObject)jsonPic)["data"]))["url"]);
                if (customer != null)
                {
                    return Json(new
                    {
                        data = customer
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    Customer customerNew = new Customer();
                    customerNew.CustomerName = fbName;
                    customerNew.FaceBookAccount = fbEmail;
                    customerNew.Email = fbEmail;
                    customerNew.PicUrl = fbpicUrl;
                    var check = customerApi.CreateCustomer(customerNew);
                    if (!check)
                    {
                        return Json(new
                        {
                            message = ConstantManager.LoginFbError
                        }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new
                    {
                        data = customerNew
                    }, JsonRequestBehavior.AllowGet);
                }
                return Json(new
                {
                    message = ConstantManager.LoginFbError

                }, JsonRequestBehavior.AllowGet);
            }
            catch
            {
                return Json(new
                {
                    message = ConstantManager.LoginFbError
                }, JsonRequestBehavior.AllowGet);
            }
            return null;
        }
        public JsonResult LoginByPhone(string fbAccessToken)
        {
            //call repository
            var customerRepo = new CustomerRepository();

            string phone = GetPhoneByAccessToken(fbAccessToken);

            var customer = customerRepo.GetCustomerByPhone(phone);
            if (customer != null)
            {
                return Json(new
                {
                    data = customer
                });
            }
            Customer customerNew = new Customer();
            customerNew.Phone = phone;
            customerNew.AccountPhone = phone;
            customerNew.Active = true;
            bool check = customerRepo.CreateCustomer(customerNew);
            if (!check)
            {
                return Json(new
                {
                    message = ConstantManager.LoginPhoneError
                });
            }
            return Json(new
            {
                data = customerNew
            });
        }
        public string GetPhoneByAccessToken(string accessToken)
        {
            string url = "https://graph.accountkit.com/v1.3/me/?access_token=" + accessToken;
            string parameter = "access_token=" + accessToken;
            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json; charset=utf-8";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();

                string jsonText;
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    jsonText = sr.ReadToEnd();
                }
                return jsonText;
            }
            catch
            {
                return "";
            }

        }
        public string GeneralCode()
        {
            Random ran = new Random();
            string result = "";
            for (int i = 0; i < 10; i++)
            {
                result += ran.Next(0, 10);
            }
            OrderRepository orderRepository = new OrderRepository();
            var checkOrder = orderRepository.GetByOrder(result);
            if (checkOrder != null)
            {
                result = GeneralCode();
            }
            return result;
        }
        public string GeneralInvoice()
        {
            Random ran = new Random();
            string result = "";
            for (int i = 0; i < 10; i++)
            {
                result += ran.Next(0, 10);
            }
            return result;
        }
        public JsonResult SetOrderTime(int storeId, int brandId, DateTime time, float kilogram, int serviceId, int petId, int customerId)
        {
            //call repository 
            TimeFrameRepository timeFrameRepo = new TimeFrameRepository();
            OrderRepository orderRepo = new OrderRepository();
            CodeRepository codeRepo = new CodeRepository();
            //result 
            var timeFrame = timeFrameRepo.GetTimeFramByServiceAndDate(serviceId, time);
            if (timeFrame == null)
            {
                return Json(new
                {
                    message = " Khung giờ đã được đặt "
                }, JsonRequestBehavior.AllowGet);
            }

            var order = new Order();
            order.BrandId = brandId;
            order.StoreId = storeId;
            order.Discount = 0;
            string invoice = GeneralInvoice();
            order.InvoiceId = invoice;
            order.Status = (int)OrderStatusEnum.New;
            //order.TimeFrame = timeFrame;
            order.TimeFrameId = timeFrame.Id;
            DateTime now = DateTime.Now;
            order.DateOrder = now;
            order.UnitPrice = timeFrame.Price;

            order.TotalAmount = timeFrame.Price;
            order.FinalAmount = timeFrame.Price;
            order.CustomerId = customerId;

            //create order
            //bool checkOrder = orderRepo.CreateOrder(order);
            //if (!checkOrder)
            //{
            //    return Json(new
            //    {
            //        message = ConstantManager.OrderGenError
            //    }, JsonRequestBehavior.AllowGet);
            //}
            //initial code 
            //var ordered = orderRepo.GetByOrder(invoice);
            Code code = new Code();
            code.Active = true;
            string invoiceCode = GeneralCode();
            code.InvoiceCode = invoiceCode;
            code.Order = order;

            // code.OrdersId = ordered.Id;
            code.Status = (int)CodeStatusEnum.New;
            bool checkCode = codeRepo.CreateCode(code);
            if (!checkCode)
            {
                return Json(new
                {
                    message = ConstantManager.CodeGenError
                }, JsonRequestBehavior.AllowGet);
            }
            return Json(new
            {
                message = ConstantManager.OrderSuccess,
                data = new
                {
                   invoice_code = code.InvoiceCode 
                    
                }
            }, JsonRequestBehavior.AllowGet);
            return null;
        }
        public JsonResult GetDetailCode()
        {

            return null;
        }
        public JsonResult GetStore()
        {
            //call repository to process 
            StoreRepository storeRepository = new StoreRepository();
            var store = storeRepository.GetAllStore();
            return Json(new
            {
                data = store
            });
        }
        // 
        ///// <summary>
        ///// get store service, time frame by storeId
        ///// </summary>
        ///// <param name="storeId">id of store </param>
        ///// <param name="serviceId">id of service</param>
        ///// <returns></returns>
        //public JsonResult GetServiceByStore(int brandId, int serviceId)
        //{
        //    //call repository to get value
        //    TimeFrameRepository tfRepository = new TimeFrameRepository();
        //    StoreRepository storeRepository = new StoreRepository();
        //    PetBrandRepository petBrandRepository = new PetBrandRepository();
        //    PetRepository petRepository = new PetRepository();
        //    //fill value 
        //    var store = storeRepository.GetStoreByBrand(brandId);

        //    var pet = petBrandRepository.GetPetBrandByBrand(brandId);

        //    var timeFrame = tfRepository.GetTimeFrameByService(serviceId);
        //    return null;
        //}
        /// <summary>
        /// to load serivce, time frame by brandId
        /// </summary>
        /// <param name="brandId"></param>
        /// <returns></returns>
        public JsonResult LoadServiceByBrand(int brandId)
        {
            //call reposioty to proces
            StoreRepository storeRepo = new StoreRepository();
            PetBrandRepository petBrandRepo = new PetBrandRepository();
            ServiceRepository serviceRepo = new ServiceRepository();
            TimeFrameRepository timeFrameRepo = new TimeFrameRepository();
            TimeRepository timeRepo = new TimeRepository();
            OptionalRepository optionalRepo = new OptionalRepository();
            //fill data
            var store = storeRepo.GetStoreByBrand(brandId);
            var pet = petBrandRepo.GetPetBrandByBrand(brandId);
            var service = serviceRepo.GetServiceByBrand(brandId);
            List<TimeFrame> timeFrameList = new List<TimeFrame>();
            List<Time> timeList = new List<Time>();
            Time time = new Time();
            if(service.Count > 0)
            {
                foreach (var item in service)
                {
                    var optional = optionalRepo.GetOptionalByService(item.Id);
                    
                    if (optional.TimeId.HasValue)
                    {
                         time = timeRepo.GetTimeById(optional.TimeId.Value);
                    }
                 
                    timeList.Add(time);
                }
            }
           
            if(timeList.Count > 0)
            {
                foreach (var item in timeList)
                {
                    var timeFrame = timeFrameRepo.GetTimeFrameByTime(item.Id);
                    if (timeFrame != null)
                    {
                        timeFrameList.Add(timeFrame);
                    }
                }
            }
            else
            {

            }
            var timeFrameDate = from t in timeFrameList
                                select new
                                {
                                    t.ToKiglogram,
                                    t.DateTime,
                                    t.Price
                                };
            var petData = from p in pet
                          select new
                          {
                              p.Name
                          };
            var storeData = from s in store
                            select new
                            {
                                s.Name,
                                s.Address
                            };
            return Json(new {
                data = new 
                {
                    timeFrameDate,
                    storeData,
                    petData
                }
            });
        }

        public JsonResult LoadInvoiceCode(int customerId)
        {
            //call repository 
            OrderRepository orderRepo = new OrderRepository();

            //fill data
            var order = orderRepo.GetByCustomer(customerId);
            var result = from o in order
                         select new
                         {
                             o.InvoiceCode,

                         };
            return Json(new
            {
                data = result
            });
        }
        public JsonResult LoadDetailOrder(int orderId)
        {

            //call repository 
            OrderRepository orderRepo = new OrderRepository();

            //fill data
            var order = orderRepo.GetByOrderId(orderId);
            Order result = new Order();
            result.InvoiceCode = order.InvoiceCode;
            result.FinalAmount = order.FinalAmount;
            result.TotalAmount = order.TotalAmount;
            string date = order.TimeFrame.DateTime.ToString();
            result.CustomerName = order.CustomerName;
            result.CustomerId = order.CustomerId;
            string phone = order.Customer.Phone;

            return Json(new
            {
                data = new  {
                    result,
                    date
                }
            });
        }
     
    }

}
