﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ManagePetSHop.Ultis
{
    public enum CustomerTypeEnum
    {
        [Display(Name = "Mới")]
        FingerScan = 0,
        [Display(Name = "Admin")]
        Admin = 1
    }
    public enum OrderStatusEnum
    {
        [Display(Name = "Mới")]
        New = 1,
        [Display(Name = "Hủy")]
        Cancel = 2,
        [Display(Name = "Hoàn thành")]
        Finish = 3,
        [Display(Name = "Đã đặt")]
        Ordered = 2
    }
    public enum CodeStatusEnum
    {
        [Display(Name = "Mới")]
        New = 1,
        [Display(Name = "Hủy")]
        Cancel = 2,
        [Display(Name = "Hoàn thành")]
        Finish = 3,
        [Display(Name = "Đã đặt")]
        Ordered = 2
    }
}